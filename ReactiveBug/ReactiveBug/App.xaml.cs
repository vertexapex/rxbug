using ReactiveBug.Pages;
using ReactiveBug.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace ReactiveBug
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // ANDROID BUG, WORKS ON IOS

            // Some background.  Not using the routing infrastructure, using an in house MVVM implementation.
            // MVVM infra left out of this project, only including the bare minimum to replicate the issue.
            // Wanted to use ISupportsActivation, to do we attached it a BasePage.
            // This worked fine in the latest v8 Alpha and broke sometime after that on Android. (doesn't work in 8.3.1)
            // Only way I know to fix it would be to not call deactivate but it defeats the purpose of using
            // WhenActivated in the first place since we've been doing it to cleanup properly.
            // Run the app and click on "click here"

            var mainPage = new MainPage();
            var vm = new MainPageViewModel(mainPage.Navigation);

            mainPage.ViewModel = vm;

            var navigationPage = new NavigationPage(mainPage);

            MainPage = navigationPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}