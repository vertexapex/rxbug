﻿using ReactiveUI;
using Xamarin.Forms;

namespace ReactiveBug.ViewModels
{
    public class ReactiveBaseModel : ReactiveObject, ISupportsActivation

    {
        public ReactiveBaseModel(INavigation navigation)
        {
            Navigation = navigation;
            Activator = new ViewModelActivator();
        }

        protected INavigation Navigation { get; }

        public ViewModelActivator Activator { get; }
    }
}