﻿     using System;
     using System.Reactive;
     using System.Reactive.Disposables;
     using System.Threading.Tasks;
     using ReactiveBug.Pages;
     using ReactiveUI;
     using Xamarin.Forms;
     
     namespace ReactiveBug.ViewModels
     {
         public class MainPageViewModel : ReactiveBaseModel
         {
             private ReactiveCommand<Unit, Unit> _labelClickCommand;
     
             public MainPageViewModel(INavigation navigation) : base(navigation)
             {
                 this.WhenActivated(d => { d(RxSetupLabelClickCommand()); });
             }
     
             public ReactiveCommand<Unit, Unit> LabelClickCommand
             {
                 get => _labelClickCommand;
                 set => this.RaiseAndSetIfChanged(ref _labelClickCommand, value);
             }
     
             private async Task InternalLabelClickCommand()
             {
                 Console.WriteLine($"{nameof(InternalLabelClickCommand)}");
     
                 var p = new TargetPage();
                 var vm = new TargetPageViewModel(p.Navigation);
     
                 p.ViewModel = vm;
     
                 await Navigation.PushAsync(p);
             }
     
             private IDisposable RxSetupLabelClickCommand()
             {
                 Console.WriteLine($"{nameof(RxSetupLabelClickCommand)}");
     
                 LabelClickCommand = ReactiveCommand
                     .CreateFromTask(InternalLabelClickCommand);     
                 LabelClickCommand
                     .IsExecuting
                     .Subscribe(isExecuting => Console.Write($"{nameof(LabelClickCommand)}.IsExecuting: {isExecuting}"));
     
                 LabelClickCommand
                     .ThrownExceptions
                     .Subscribe(exception =>
                         Console.WriteLine($"Error executing {nameof(LabelClickCommand)}.  Ex: {exception.ToString()}"));
     
                 return LabelClickCommand;
             }
         }
     }