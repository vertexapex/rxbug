﻿using System;
using System.Reactive.Disposables;
using ReactiveUI;
using Xamarin.Forms;

namespace ReactiveBug.ViewModels
{
    public class TargetPageViewModel : ReactiveBaseModel
    {
        public TargetPageViewModel(INavigation navigation) : base(navigation)
        {
            this.WhenActivated((CompositeDisposable d) =>
                Console.WriteLine($"{nameof(TargetPageViewModel)}.WhenActivated"));
        }
    }
}