﻿     using System;
     using ReactiveBug.ViewModels;
     using ReactiveUI;
     using ReactiveUI.XamForms;
     using Xamarin.Forms;
     
     namespace ReactiveBug.Pages
     {
         public class BaseContentPage<T> : ReactiveContentPage<T> where T : ReactiveBaseModel
         {
             public BaseContentPage()
             {
                 this.Events().Appearing.Subscribe(args =>
                 {
                     if (ViewModel is ISupportsActivation activation)
                     {
                         Console.WriteLine($"Activating {GetType().Name}");
     
                         activation.Activator?.Activate();
                     }
                 });
     
                 this.Events().Disappearing.Subscribe(args =>
                 {
                     if (ViewModel is ISupportsActivation activation)
                     {
                         Console.WriteLine($"Deactivating {GetType().Name}");
     
                         activation.Activator.Deactivate();
                     }
                 });
             }
         }
     }