﻿using ReactiveBug.ViewModels;
using Xamarin.Forms.Xaml;

namespace ReactiveBug.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TargetPage : BaseContentPage<TargetPageViewModel>
    {
        public TargetPage()
        {
            InitializeComponent();
        }
    }
}