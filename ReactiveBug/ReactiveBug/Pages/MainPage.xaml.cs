﻿using ReactiveBug.ViewModels;

namespace ReactiveBug.Pages
{
    public class MainPage : BaseContentPage<MainPageViewModel>
    {
        public MainPage()
        {
            InitializeComponent();
        }
    }
}